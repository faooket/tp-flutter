import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Accueil'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            /*SvgPicture.asset(
              "assets/images/logo.svg"
              Color: Colors.blue,
            ),*/
            Row(children: [
              Expanded(flex:2, child: Image.asset("../assets/images/image1.jpg"),),
              Expanded(flex:2, child: Image.network("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSfYh5e-oN2LAxUophTthsRwATlQDpK-FZdxP2__KCCxnuAAcsq1kAVVV5rxNwQC462RNY&usqp=CAU"),),
            ],),
            /*const Text(
              "OKETOKOUN Faozâne",
              style: TextStyle(fontSize: 42, fontFamily: 'Poppins'),
            ),*/
            const Text(
              "OKETOKOUN Faozâne",
              style: TextStyle(
                fontSize: 24,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
