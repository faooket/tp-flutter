import 'package:flutter/material.dart';
import 'package:tpflutter/pages/home_page.dart';
import 'package:tpflutter/pages/login_page.dart';
import 'package:tpflutter/pages/setting_page.dart';
//import 'package:flutter_svg/flutter_svg.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  static const Color _blueColor = Colors.blue;
  static const Color _redColor = Colors.red;
  static const Color _greenColor = Colors.green;

  int _currentIndex = 0;

  void setCurrentIndex(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: [HomePage(), LoginPage(), SettingPage()][_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          onTap: setCurrentIndex,
          type: BottomNavigationBarType.fixed,
          iconSize: 32,
          elevation: 10,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home, color: _blueColor),
              label: 'Accueil',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.login, color: _redColor),
              label: 'Connexion',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings, color: _greenColor),
              label: 'Paramètre',
            ),
          ],
        ),
      ),
    );
  }
}
